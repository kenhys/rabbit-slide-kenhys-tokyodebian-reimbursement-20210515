# DebianプロジェクトのReimbursementを\\n利用する方法

subtitle
:  Debianプロジェクトの支援制度を活用してみよう

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2021年5月 東京エリア・関西合同Debian勉強会

allotted-time
:  15m

theme
:  .

# スライドは公開済みです

* Debianプロジェクトの\\nReimbursementを利用する方法
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-reimbursement-20210515/>

# プロフィール

![](images/profile.png){:relative-height="60" align="right"}

* ひよこDebian Developer @kenhys
* トラックポイント(ソフトドーム派)
* わさビーフ(わさっち派)

# 本日の内容

* DebianプロジェクトのReimbursementの仕組みについて紹介
  * どんな仕組みか?
  * 誰が申請できるの?
  * 手続きはどうするの?
  * どれくらいかかるの?

# どんな仕組みか?

* Debianプロジェクトが諸経費を支援してくれる制度

  * <https://wiki.debian.org/Teams/DPL/Reimbursement>
  * 全額 or 一部補助を申請できる
  * 後日かかった費用を精算する 💰
    * DebConfに参加するための費用
    * 資材の購入費
  
# 誰が申請できるのか?

* Debian Developer
* Debian Developerの支持をとりつけた人
  * メールをDDに署名してもらうなどの必要あり

# 手続きはどうするの?

* DPLに事前にお伺いをたてる(見積もり)
* DPLにOKもらったら何某かを購入する
* Trusted Organizationに書類を揃えて提出する
* 事務処理をひたすら待つ

# Debianのお金の話

* プロジェクトになされた寄付は資産管理団体(Trusted Organization)で扱う
  * debian.ch (スイス)
  * Debian France (ヨーロッパ)
  * **SPI Inc.** (その他) 日本からの申請はココ
* ReimbursementはTOで処理される

# DPLに事前承認をもらう

* DPLにメールを投げる
  * TO: leader@debian.org
  * CC: treasurer@debian.org
  * CC: treasurer@rt.spi-inc.org
* 物品の目録とかかる費用を連絡する
* SPI Inc.のトラッキング用に[TREASURER #NNNN]という対応するチケットが作られる

# RT チケットを作成する

* Debian側のトラッキング用のチケット
* 件名にDebian RTを入れてtreasurer-spi@rt.debian.orgにメールする
  * CC: treasurer@rt.spi-inc.org
* Treasurerチームが管轄
  <https://wiki.debian.org/Teams/Treasurer>

# 必要な書類を準備する

* SPI Inc.の場合
  * SPI Reimbursement FormでPDFを作成する
  * Expense Reportを作成する
  * 領収書を揃える(PDF)
  * 提出用のPDFを作成する

# SPI Reimbursement Form

* フォームに必要事項を入力してPDFを生成できる
  * <https://www.spi-inc.org/treasurer/reimbursement-form/>
* 住所とか口座情報とか
  * IBAN
    * 不要。日本では使われていないため
  * SWIFT/BIC Code
    * 銀行のサイト参照。海外からの送金の受取方法を確認するとよい


# Expense Reportを作成する

![](images/xe-travel-expenses-calculator.png){:relative-height="70"}

* 指定した日のレートで金額を計算してPDF生成

  * <https://www.xe.com/travel-expenses-calculator/>


# 提出用のPDFを作成する

* SPI Inc.の指定するSubmission Package
  * 1ページ目: SPI Reimbursement FormのPDF
  * 2ページ目: Expense ReportのPDF
  * 3ページ目以降: 領収書のPDF

* treasurer@rt.spi-inc.org宛に送付

# どれくらいかかるのか?

* 申請例: YubiKeyの購入費用 🔐
  * 2021/03/12 DPLに申請
  * 2021/03/15 DPL承認済み
  * 2021/03/15 申請書類一式をSPI Inc.に送付
  * 2021/03/23 申告不備について確認あり
  * 2021/05/01 進捗状況の確認依頼
  * 2021/05/02 入金処理

# 利用してみてどうだったか

* メールベースの仕組みが面倒くさい 🤨
  * wikiを熟読すべきではある
* 複数組織にまたがるチケット 😦
  * 一元化してあるといいのに
* 進捗状況がわかりづらい 😞

# こうあって欲しい願望

* nm.d.oみたいにWebサービス化
  * 機械的なチェックをかける(申請ミス、書類不備を防止)
  * rt.debian.orgとrt.spi-inc.orgのやりとりはバックエンド処理でみせない
* 新たにシステム化するコストに見合うか? 🤔
  * TOごとの制限事項の調整がとても大変そう 😵
  * <https://wiki.debian.org/Teams/Treasurer/Brainstorming>

# まとめ

* DebianプロジェクトにはReimbursementという仕組みがあります
* 申請が放置されていそうな気がしたら確認しといたほうがよい
* 少々手間はかかるが機会があれば利用するのをおすすめ
